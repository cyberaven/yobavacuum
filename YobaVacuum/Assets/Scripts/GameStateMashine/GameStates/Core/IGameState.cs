﻿public interface IGameState
{
    EnumGameStateName GetName();
    void Run();
    void Stop();
}