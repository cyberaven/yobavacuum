﻿using UnityEngine;

public abstract class GameState : MonoBehaviour, IGameState
{
    [SerializeField] private EnumGameStateName Name;

    public EnumGameStateName GetName()
    {
        return Name;
    }

    virtual public void Run()
    {
        throw new System.Exception("Dont set Run() in GameState " + Name);
    }

    virtual public void Stop()
    {
        throw new System.Exception("Dont set Stop() in GameState " + Name);
    }
}