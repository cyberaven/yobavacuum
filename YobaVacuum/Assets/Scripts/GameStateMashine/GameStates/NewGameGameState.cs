﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameGameState : GameState
{
    public override void Run()
    {       
        GameDataManager.Instance.CreateNew(0f,1f); ///manual power
        GameStateMashine.Instance.SetState(EnumGameStateName.ShowStarMap);        
    }
    public override void Stop()
    {   
    }
}
