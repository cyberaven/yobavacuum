﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanStarSystemGameState : GameState
{
    public override void Run()
    {
        GameUI.Instance.PanelsManager.Show(EnumPanelName.ScanStarSystemGame);
    }
    public override void Stop()
    {
        
    }
}
