﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowStarMapState : GameState
{
    public override void Run()
    {
        GameUI.Instance.PanelsManager.Show(EnumPanelName.StarMap);
    }
    public override void Stop()
    {        
    }
}
