﻿using UnityEngine;
using System.Collections.Generic;

public class GameStateMashine : MonoBehaviour
{
    public static GameStateMashine Instance;

    [SerializeField] private GameState CurrentState;
    [SerializeField] private List<GameState> StatePrefabs = new List<GameState>();
    private List<IGameState> States = new List<IGameState>();

    private void Awake()
    {
        Instance = this;
        CreateAllStates();
    }
    private void CreateAllStates()
    {
        foreach (GameState gameState in StatePrefabs)
        {
            GameState gs = Instantiate(gameState, transform);
            States.Add(gs);
        }
    }

    public void SetState(EnumGameStateName name)
    {       
        foreach (IGameState gameState in States)
        {
            if(gameState.GetName() == name)
            {
                if(CurrentState != null)
                {
                    CurrentState.Stop();
                }                
                CurrentState = gameState as GameState;
                CurrentState.Run();
            }
        }
    }
}