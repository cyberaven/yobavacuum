﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StarSystemObjPool : MonoBehaviour
{
    [SerializeField] private List<Sprite> sprites = new List<Sprite>();

    public static StarSystemObjPool Instance;

    [SerializeField] private List<StarSystemView> StarSystemViews = new List<StarSystemView>();
    private List<StarSystemView> CurrentStarSystemViews = new List<StarSystemView>();

    private void Awake()
    {
        Instance = this;
    }

    public StarSystemView GetStarSystemView()
    {
        if(CurrentStarSystemViews.Count > 0)
        {
            foreach(StarSystemView starSystemView in CurrentStarSystemViews)
            {
                if (starSystemView.gameObject.activeSelf == false)
                {
                    starSystemView.gameObject.SetActive(true);
                    return starSystemView;
                }
                else
                {
                    return CreateStarSystemView();
                }
            }
        }
        else
        {
            return CreateStarSystemView();
        }

        throw new System.Exception("StarSystemObjPool GetStarSystemView Error");
    }
    private StarSystemView CreateStarSystemView()
    {
        

        StarSystemView starSystemView = Instantiate(StarSystemViews[0], transform);

        //убрать
        starSystemView.gameObject.GetComponent<Image>().sprite = sprites[Random.Range(0, sprites.Count)];
        //убрать

        CurrentStarSystemViews.Add(starSystemView);
        return starSystemView;
    }

}
