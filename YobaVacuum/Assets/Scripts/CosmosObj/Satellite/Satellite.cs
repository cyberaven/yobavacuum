﻿public class Satellite : CosmosObj
{
    private SatelliteType SatelliteType;

    public Satellite(StarSystem starSystem, CosmosObj parent, CosmosObjType cosmosType, SatelliteType satelliteType) : base(starSystem, parent, cosmosType)
    {
        SatelliteType = satelliteType;
    }
}