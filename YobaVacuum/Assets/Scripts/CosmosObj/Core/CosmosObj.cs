﻿using UnityEngine;
using System.Collections;
using UnityEngine.Animations;

public abstract class CosmosObj : ICosmosObj
{
    public StarSystem StarSystem { get; }
    public CosmosObj Parent { get; }
    public CosmosObjType CosmosObjType { get; }
    public bool IsScanned { get; set; }

    public CosmosObj(StarSystem starSystem, CosmosObj parent, CosmosObjType type)
    {
        StarSystem = starSystem;
        Parent = parent;
        CosmosObjType = type;
        IsScanned = false;
    }    
}
