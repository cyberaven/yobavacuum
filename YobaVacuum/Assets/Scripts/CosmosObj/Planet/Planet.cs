﻿using System.Collections.Generic;

public class Planet
{
    private EnumPlanetType Type;

    private StarSystem StarSystem;
    private List<Satellite> Satellites = new List<Satellite>();

    public Planet(EnumPlanetType type)
    {
        Type = type;
    }

    public void SetStarSystem(StarSystem starSystem)//hardCode!!!
    {
        StarSystem = starSystem;
    }
    public void SetSatellites(List<Satellite> satellites)
    {
        Satellites = satellites;
    }
    public List<Satellite> GetSatellites()
    {
        return Satellites;
    }
    public void SetSatellite(Satellite satellite)
    {
        Satellites.Add(satellite);
    }

}