﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class StarSystemView : MonoBehaviour, IPointerClickHandler
{
    private StarSystem StarSystem;

    public void OnPointerClick(PointerEventData eventData)
    {   
        if(GameUI.Instance.PanelsManager.GetPanel(EnumPanelName.StarMap).gameObject.activeSelf == true)
        {
            GameDataManager.Instance.GetCurrent().SelectedStarSystemForScaning = StarSystem;
            GameStateMashine.Instance.SetState(EnumGameStateName.ScanStarSystemGame);
        }
    }

    public void SetStarSystem(StarSystem starSystem)
    {
        StarSystem = starSystem;
    }
}
