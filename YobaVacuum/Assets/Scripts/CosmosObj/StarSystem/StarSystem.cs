﻿using System.Collections.Generic;

public class StarSystem
{    
    private List<Star> Stars = new List<Star>();
    private List<Planet> Planets = new List<Planet>();
    private List<Satellite> Satellites = new List<Satellite>();
        
    public void SetStars(List<Star> stars)//hardCode!!!!!!!!
    {
        Stars = stars;
    }
    public List<Star> GetStars()
    {
        return Stars;
    }
    public void AddStar(Star star)//hardCode!!!!!!!!
    {
        Stars.Add(star);
    }

    public void SetPlanets(List<Planet> planets)//hardCode!!!!!!!!
    {
        Planets = planets;
    }
    public List<Planet> GetPlanets()
    {
        return Planets;
    }
    public void SetPlanet(Planet planet)//hardcode!!!!!
    {
        Planets.Add(planet);
    }
    public void SetSatellites(List<Satellite> satellites)//hardCode!!!!!!!!
    {
        Satellites = satellites;
    }
}