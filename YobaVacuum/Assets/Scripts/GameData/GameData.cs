﻿using System.Collections.Generic;

public class GameData
{
    //private GameEvents
    //private Time
    //private Ship

    public StarSystem SelectedStarSystemForScaning { get; set; }

    private List<StarSystem> StarSystems = new List<StarSystem>();
    private List<Star> Stars = new List<Star>();
    private List<Planet> Planets = new List<Planet>();
    private List<Satellite> Satellites = new List<Satellite>();

    public GameData(List<StarSystem> starSystems, List<Star> stars, List<Planet> planets, List<Satellite> satellites)
    {        
        StarSystems = starSystems;
        Stars = stars;
        Planets = planets;
        Satellites = satellites;

        UnityEngine.Debug.Log("GameData Created. StarSystems: " + StarSystems.Count + " Stars: " + Stars.Count + " Planets: " + Planets.Count + " Satellite: " + Satellites.Count);
    }

    public List<StarSystem> GetStarSystems()
    {
        return StarSystems;
    }
   
}
