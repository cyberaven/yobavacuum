﻿public interface IGameDataFactory
{
    GameData Create(EnumGameDataType type, float minStarSystemsCount, float maxStarSystemsCount);
}