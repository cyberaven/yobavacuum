﻿using UnityEngine;

public class GameDataManager : MonoBehaviour
{
    public static GameDataManager Instance;
    [SerializeField] private GameData CurrentGameData;
    
    private IGameDataFactory GameDataFactory;

    private void Awake()
    {
        Instance = this;
        GameDataFactory = new SimpleGameDataFactory();
    }

    public void CreateNew(float minSolarSystemCount, float maxSolarSystemCount)
    {
        GameData gameData = GameDataFactory.Create(EnumGameDataType.Random, minSolarSystemCount, maxSolarSystemCount);
        CurrentGameData = gameData;
    }    
    public GameData GetCurrent()
    {
        return CurrentGameData;
    }
}