﻿using System;
using System.Collections.Generic;

public class SimpleGameDataFactory : GameDataFactory
{
    //вынести в файл настроек
    private int MaxStarCount = 4; //in one star system
    private int MinStarCount = 0;

    private int MaxPlanetCount = 4; //in one star system
    private int MinPlanetCount = 0;

    private int MaxSatelliteCount = 8; //in one planet
    private int MinSatelliteCount = 0;
    //вынести в файл настроек

    public SimpleGameDataFactory()
    {
    }

    //перебрать весь завод. Создание звезд спутников и планет вынести
    public override GameData Create(EnumGameDataType type, float minStarSystemsCount, float maxStarSystemsCount)
    {
        if (type == EnumGameDataType.Random)
        {
            float starSystemCount = UnityEngine.Random.Range(minStarSystemsCount, maxStarSystemsCount);

            List<StarSystem> starSystems = CreateStarSystem(starSystemCount * 100f);//умножение на 100, чтобы 0,654 перевести в 65,4
            List<Star> stars = CreateRandStars(starSystems);
            List<Planet> planets = CreateRandPlanets(starSystems);
            List<Satellite> satellites = CreateRandSatellites(planets);

            GameData result = new GameData(starSystems, stars, planets, satellites);
            return result;
        }

        throw new Exception("No GameData Type: " + type);
    }
    
    private List<StarSystem> CreateStarSystem(float count)
    {
        List<StarSystem> result = new List<StarSystem>();

        for (int i = 0; i < count; i++)
        {
            StarSystem starSystem = new StarSystem();
            result.Add(starSystem);
        }

        return result;
    }

    private List<Star> CreateRandStars(List<StarSystem> starSystems)
    {
        List<Star> result = new List<Star>();

        for (int i = 0; i < starSystems.Count; i++)
        {
            int starCount = UnityEngine.Random.Range(MinStarCount, MaxStarCount);

            for (int j = 0; j < starCount; j++)
            {
                Star star = CreateRandStar(starSystems[i]);
                starSystems[i].AddStar(star);
                result.Add(star);
            }
        }

        return result;
    }

    private Star CreateRandStar(StarSystem starSystem)
    {           
        EnumStarType starType = (EnumStarType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(EnumStarType)).Length);
        Star star = new Star(starType);
        star.SetStarSystem(starSystem);
        return star;
    }

    private List<Planet> CreateRandPlanets(List<StarSystem> starSystems)
    {
        List<Planet> result = new List<Planet>();

        for (int i = 0; i < starSystems.Count; i++)
        {
            int planetCount = UnityEngine.Random.Range(MinPlanetCount, MaxPlanetCount);

            for (int j = 0; j < planetCount; j++)
            {
                Planet planet = CreateRandPlanet(starSystems[i]);
                starSystems[i].SetPlanet(planet);
                result.Add(planet);
            }
        }

        return result;
    }
    private Planet CreateRandPlanet(StarSystem starSystem)
    {
        EnumPlanetType planetType = (EnumPlanetType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(EnumPlanetType)).Length);

        Planet planet = new Planet(planetType);
        planet.SetStarSystem(starSystem);
        return planet;
    }

    private List<Satellite> CreateRandSatellites(List<Planet> planets)
    {
        List<Satellite> result = new List<Satellite>();
               
        for (int j = 0; j < planets.Count; j++)
        {
            int satelliteCount = UnityEngine.Random.Range(MinSatelliteCount, MaxSatelliteCount);

            for (int k = 0; k < satelliteCount; k++)
            {
                Satellite satellite = CreateRandSatellite();
                planets[j].SetSatellite(satellite);
                result.Add(satellite);
            }
        }        

        return result;
    }

    private Satellite CreateRandSatellite()
    {
        EnumSatelliteType satelliteType = (EnumSatelliteType)UnityEngine.Random.Range(0, Enum.GetNames(typeof(EnumSatelliteType)).Length);
        Satellite satellite = new Satellite();
        Satellite satellite = new Satellite(satelliteType);       
        return satellite;
    }

}