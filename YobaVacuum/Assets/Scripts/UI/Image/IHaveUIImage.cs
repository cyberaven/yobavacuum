﻿using UnityEngine;

public interface IHaveUIImage
{
    void SetImage(Sprite sprite);

}