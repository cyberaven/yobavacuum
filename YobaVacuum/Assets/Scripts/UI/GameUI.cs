﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public static GameUI Instance;

    [Header("Секция настроек игры")]
    [SerializeField] public float MaxSystemCount = 1;
    [SerializeField] public float MinSystemCount = 0;

    [Header("Секция Панелей")]    
    [SerializeField] private Transform PanelFolder;
    [SerializeField] private EnumPanelName FirstShowPanel;
    [SerializeField] private List<Panel> PanelPrefabs;
    public PanelsManager PanelsManager { get; private set; }



    private void Awake()
    {
        Initilization(); //это первый конструктор
    }

    void Initilization()
    {
        //FirstConstructor тут
        Instance = this;

        Canvas canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
        canvas.sortingLayerName = "MyUI";

        PanelsManager = new PanelsManager(PanelPrefabs, FirstShowPanel, PanelFolder);
        //FirstConstructor тут

    }

}
