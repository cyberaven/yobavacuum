﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PanelsManager
{
    private List<Panel> PanelPrefabs = new List<Panel>();
    private List<IPanel> Panels = new List<IPanel>();
    private Transform PanelFolder = null;
    public Stack<IPanel> LastOpenedPanelsStack = new Stack<IPanel>();

    public PanelsManager(List<Panel> panelPrefabs, EnumPanelName firstShowPanel, Transform panelFolder)
    {
        PanelPrefabs = panelPrefabs;
        PanelFolder = panelFolder;

        CreateAllPanel();
        HideAll();
        Show(firstShowPanel);
    }

    private void CreateAllPanel()
    {
        foreach (Panel panel in PanelPrefabs)
        {
            Panel p = UnityEngine.Object.Instantiate(panel, PanelFolder);
            Panels.Add(p);
        }
    }
    public void HideAll()
    {
        foreach (Panel panel in Panels)
        {
            panel.Hide();
        }
    }
    public void Show(EnumPanelName name, bool hideAll = true)
    {        
        foreach (IPanel panel in Panels)
        {
            if (panel.GetPanelName() == name)
            {
                Debug.Log("Panel Show: " + name);
                panel.Show();
                LastOpenedPanelsStack.Push(panel);
            }
            else
            {                
                if (hideAll == true)
                {
                    panel.Hide();
                }
            }
        }
    }
    public void ShowPreviousPanel()
    {
        LastOpenedPanelsStack.Pop();
        IPanel panel = LastOpenedPanelsStack.Peek();
        Show(panel.GetPanelName());
    }

    public void Hide(EnumPanelName name)
    {
        foreach (IPanel panel in Panels)
        {
            if (panel.GetPanelName() == name)
            {
                panel.Hide();
            }
        }
    }
    public Panel GetPanel(EnumPanelName name)
    {
        foreach (IPanel panel in Panels)
        {
            if (panel.GetPanelName() == name)
            {
                return panel as Panel;
            }
        }

        throw new System.Exception("No panel witn name: " + name);
    }

}
