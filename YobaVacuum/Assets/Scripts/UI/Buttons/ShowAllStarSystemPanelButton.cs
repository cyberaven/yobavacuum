﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowAllStarSystemPanelButton : MonoBehaviour, IPointerClickHandler
{
     public void OnPointerClick(PointerEventData eventData)
    {        
        GameUI.Instance.PanelsManager.Show(EnumPanelName.AllStarSystems);
    }
}
