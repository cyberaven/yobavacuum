﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.EventSystems;

public class DownloadFileButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private string URL;
    [SerializeField] private string Path;

    public void OnPointerClick(PointerEventData eventData)
    {
        DownloadFile(URL, Path);
    }
    //https://drive.google.com/file/d/1qfJZglsC_VMDuzoJ0N-LuAas0itRS0OA/view?usp=sharing
    //1qfJZglsC_VMDuzoJ0N-LuAas0itRS0OA
    //https://drive.google.com/uc?export=download&id=
    //https://drive.google.com/uc?export=download&id=1qfJZglsC_VMDuzoJ0N-LuAas0itRS0OA
    private void DownloadFile(string url, string path)
    {
        WebClient webClient = new WebClient();
        webClient.DownloadFile(url, Application.dataPath + "/" + path);
    }
}
