﻿using UnityEngine;
using System.Collections;
using System;

public interface IPanel
{   
    EnumPanelName GetPanelName();
    void Show();
    void Hide();
}
