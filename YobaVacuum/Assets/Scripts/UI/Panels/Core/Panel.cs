﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Panel : MonoBehaviour, IPanel
{
    [SerializeField] private EnumPanelName Name;

    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public EnumPanelName GetPanelName()
    {
        return Name;
    }
}
