﻿using UnityEngine;
using System.Collections;

public enum EnumPanelName
{    
    Settings,    
    MainMenu,
    PauseMenu,    
    Authors,
    HowToPlay,    
    Intro,   
    LoadingScreen,
    StarMap,
    AllStarSystems,
    ScanStarSystemGame
}
