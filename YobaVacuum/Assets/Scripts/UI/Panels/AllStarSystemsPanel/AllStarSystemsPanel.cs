﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AllStarSystemsPanel : Panel
{
    [SerializeField] private Transform StarSystemRowFolder;
    [SerializeField] private StarSystemRow StarSystemRowPrefab;
    private List<StarSystemRow> CurrentStarSystemRows = new List<StarSystemRow>();
    private GameData GameData;


    private void Start()
    {
        GameData = GameDataManager.Instance.GetCurrent();
        FillTable();
    }

    private void FillTable()
    {
        List<StarSystem> starSystems = GameData.GetStarSystems();
        foreach (StarSystem starSystem in starSystems)
        {
            StarSystemRow starSystemRow = Instantiate(StarSystemRowPrefab, StarSystemRowFolder);
            starSystemRow.transform.localScale = new Vector3(1, 1, 1);
            starSystemRow.SetStarSystem(starSystem);
            CurrentStarSystemRows.Add(starSystemRow);
        }
    }

}
