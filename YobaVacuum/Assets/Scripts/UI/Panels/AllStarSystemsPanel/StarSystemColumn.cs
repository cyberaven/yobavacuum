﻿using UnityEngine;
using System.Collections;
using UnityEngine.PlayerLoop;

public class StarSystemColumn : MonoBehaviour
{
    [SerializeField] private StarView StarViewPrefab;
    [SerializeField] private PlanetView PlanetViewPrefab;
    [SerializeField] private SatelliteView SatelliteViewPrefab;
    
    private Star Star;
    private Planet Planet;
    private Satellite Satellite;

    public void SetStar(Star star)
    {
        Star = star;
        StarView starView = Instantiate(StarViewPrefab, transform);
        starView.SetStar(Star);
    }
    public void SetPlanet(Planet planet)
    {
        Planet = planet;
        PlanetView planetView = Instantiate(PlanetViewPrefab, transform);
        planetView.SetPlanet(Planet);
    }

    public void SetSatellite(Satellite satellite)
    {
        Satellite = satellite;
        SatelliteView satelliteView = Instantiate(SatelliteViewPrefab, transform);
        satelliteView.SetSatellite(satellite);
    }
}

