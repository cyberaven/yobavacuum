﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSystemRow : MonoBehaviour
{
    private StarSystem StarSystem;
    [SerializeField] private StarSystemColumn StarSystemColumn;


    public void SetStarSystem(StarSystem starSystem)
    {
        StarSystem = starSystem;
        FillRow();
    }

    private void FillRow()
    {
        List<Star> stars = StarSystem.GetStars();
        List<Planet> planets = StarSystem.GetPlanets();

        foreach(Star star in stars)
        {
            StarSystemColumn starSystemColumn = Instantiate(StarSystemColumn, transform);
            starSystemColumn.SetStar(star);
        }
        foreach (Planet planet in planets)
        {
            StarSystemColumn starSystemColumn = Instantiate(StarSystemColumn, transform);
            starSystemColumn.SetPlanet(planet);

            List<Satellite> satellites = planet.GetSatellites();
            foreach(Satellite satellite in satellites)
            {
                starSystemColumn.SetSatellite(satellite);
            }
        }
    }
}
