﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarMapPanel : Panel
{
    private GameData GameData;
    private StarSystemObjPool StarSystemObjPool;

    [SerializeField] private Transform StarSystemsFolder;

    private void Start()
    {
        GameData = GameDataManager.Instance.GetCurrent();
        StarSystemObjPool = StarSystemObjPool.Instance;

        ShowStarSystem();
    }

    private void ShowStarSystem()
    {
        List<StarSystem> starSystems = GameData.GetStarSystems();

        for (int i = 0; i < starSystems.Count; i++)
        {
            StarSystemView starSystemView = StarSystemObjPool.GetStarSystemView();
            starSystemView.SetStarSystem(starSystems[i]);
            starSystemView.transform.SetParent(StarSystemsFolder);
            starSystemView.transform.localScale = new Vector3(1,1,1);
        }

    }
}
