﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPanel : Panel
{
    [SerializeField] private EnumPanelName NextPanel;
    [SerializeField] private int ThroughtTime = 5;
    private float CurrentTime = 0;

    private void Update()
    {
        ShowInTime();            
    }

    private void ShowInTime()
    {
        CurrentTime += Time.deltaTime;

        if(CurrentTime > ThroughtTime)
        {
            GameUI.Instance.PanelsManager.Show(NextPanel);
        }
    }
}
