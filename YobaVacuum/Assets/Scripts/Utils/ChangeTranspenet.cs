﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeTranspenet : MonoBehaviour
{
    private Text Text;

    private void Start()
    {        
        Text = GetComponent<Text>();
    }
    private void FixedUpdate()
    {
        if (Text.color.a < 1)
        {
            Color color = new Color();
            color.a = Mathf.Lerp(Text.color.a, 1, Time.fixedDeltaTime * 0.25f);
            color.r = Text.color.r;
            color.g = Text.color.g;
            color.b = Text.color.b;
            Text.color = color;
        }
    }
}
