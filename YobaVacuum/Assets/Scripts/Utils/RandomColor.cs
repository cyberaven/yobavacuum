﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomColor : MonoBehaviour
{
    [SerializeField] private Image Image;
    [SerializeField] private Color Color;

    private void Start()
    {
        SetRandColor();
    }

    private void SetRandColor()
    {
        Color = new Color();
        Color.a = 1f;
        Color.r = Random.Range(0f, 1f);
        Color.g = Random.Range(0f, 1f);
        Color.b = Random.Range(0f, 1f);
        Image.color = Color;        
    }
}
